#include <stdio.h>
#include <math.h>
#include <SDL.h>
#include <SDL_video.h>
#include <SDL_image.h>

int main(int argc, char **argv){
	SDL_Window *window;
	SDL_Renderer *renderer;
	SDL_Surface *loadingSurface;
	SDL_Texture *texture;
	SDL_Event event;
	SDL_DisplayMode mode;
	char *filename;
	float scale, imageSize, screenSize;

	// src is for zoom functionality, dst should fill the whole window
	SDL_Rect src, dst;

	if (argc < 2){
		printf("Usage: %s [image]\n", argv[0]);
		return 1;
	}

	for (int i = 1; i < argc; i++){
		switch (argv[i][0]){
		case '-':
			switch (argv[i][1]){
			case 'h':
				printf("Usage: %s [image]\n", argv[0]);
				return 0;
			}
		default:
			filename = argv[i];
		}
	}
	
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS) != 0){
		fprintf(stderr, "SDL_Init failed: %s\n", SDL_GetError());
		return 1;
	}
	IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG);

	atexit(SDL_Quit);
	atexit(IMG_Quit);

	SDL_GetCurrentDisplayMode(0, &mode);
	window = SDL_CreateWindow("Lightview", SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED, mode.w, mode.h,
		SDL_WINDOW_RESIZABLE);
	if (window == NULL){
		fprintf(stderr, "SDL_CreateWindow failed: %s\n",
			SDL_GetError());
		return 1;
	}

	renderer = SDL_CreateRenderer(window, -1, 0);
	if (renderer == NULL){
		fprintf(stderr, "Renderer creation failed: %s\n",
			SDL_GetError());
		return 1;
	}

	loadingSurface = IMG_Load(filename);
	if (loadingSurface == NULL){
		fprintf(stderr, "Loading image failed: %s\n", IMG_GetError());
		return 1;
	}

	texture = SDL_CreateTextureFromSurface(renderer, loadingSurface);

	src.x = 0;
	src.y = 0;
	src.w = loadingSurface->w;
	src.h = loadingSurface->h;
	if (src.w > mode.w){
		scale = (float) mode.w / (float) src.w;
	}
	if (src.h > mode.h){
		scale = (float) mode.h / (float) src.h;
	}
	if (src.h <= mode.h && src.w <= mode.w){
		imageSize = sqrtf(powf((float) src.w, 2.0d) +
			    powf((float) src.h, 2.0d));
		screenSize = sqrtf(powf((float) mode.w, 2.0d) +
			     powf((float) mode.h, 2.0d));
		scale = screenSize / imageSize;
	}
	if (scale > 2.0d)
		scale = 2.0d;
	dst.x = (int) (((float) mode.w - ((float) src.w * scale)) / 2.0d);
	dst.y = (int) (((float) mode.h - ((float) src.h * scale)) / 2.0d);
	if (dst.x < 0){
		dst.x = 0;
		if ((float) src.w * scale > (float) mode.w)
			scale = (float) mode.w / (float) src.w;
		dst.x = (int) (((float) mode.w - ((float) src.w * scale)) / 2.0d);
	}
	if (dst.y < 0){
		dst.y = 0;
		if ((float) src.h * scale > (float)mode.h)
			scale = (float) mode.h / (float) src.h;
		dst.y = (int) (((float) mode.h - ((float) src.h * scale)) / 2.0d);
	}
	dst.w = (int) ((float) src.w * scale);
	dst.h = (int) ((float) src.h * scale);

	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
	SDL_RenderClear(renderer);
	SDL_RenderCopy(renderer, texture, &src, &dst);
	SDL_RenderPresent(renderer);

	SDL_WaitEvent(&event);
	while (event.type != SDL_KEYUP)
		SDL_WaitEvent(&event)
		;
	
	SDL_FreeSurface(loadingSurface);
	SDL_DestroyWindow(window);
	SDL_Quit();
	return 0;
}
