### Lightview

Lightview is a simple and lightweight(ish) image viewer useful for viewing
images from the command line, especially for tiling wm users.

Lightview is built using SDL2 library as its only dependency.

## Usage

Lightview is simple to use:
```
$ lightview [filename]
```
Once Lightview is started, you can look at the picture and close it by pressing
any key on the keyboard. There is currently no other functionality.
