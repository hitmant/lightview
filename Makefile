cc = gcc
cflags = -Wall

lightview : main.o
	$(cc) -o lightview main.o `sdl2-config --libs` -lSDL2_image -lm

main.o : main.c
	$(cc) $(cflags) -c main.c `sdl2-config --cflags`

install : lightview
	sudo mv lightview /usr/bin/lightview
